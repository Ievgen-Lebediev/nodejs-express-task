const { user } = require('../models/user');
const {body, validationResult} = require('express-validator');

const userValidationRules = () => {
    return [
      body('email').not().isEmpty().isEmail().matches(/gmail.com$/),
      body('firstName').not().isEmpty(),
      body('secondName').not().isEmpty(),
      body('phoneNumber').not().isEmpty().matches( /\+380\d{9}/),
      body('password').isLength({ min: 3 })
    ]
  }

const createUserValid = (req, res, next) => {
    const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next()
  }

  return res.status(400).json( { 
    errors: errors.array(),
    message: 'Incorrect registration data'
})
}

const updateUserValid = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next()
    }
  
    return res.status(400).json( { 
      errors: errors.array(),
      message: 'Incorrect registration data'
  })
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.userValidationRules = userValidationRules;