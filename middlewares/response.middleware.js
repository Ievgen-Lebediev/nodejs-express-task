const responseMiddleware = (req, res, next) => {
    if (res.err) {
        let statusErr = res.err.status;
        console.log(statusErr, res.err.message)
        res.status(statusErr).json({error: true, message: res.err.message});
    } else {
        res.status(200).send(res.data);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;