const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid, userValidationRules } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res) => {
    try {
        if (!res.err) {
            res.data = UserService.getAllUsers(req.body);
        }
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    }
}, responseMiddleware);

router.get('/:id', (req, res) => {
    try {
        if (!res.err) {
            res.data = UserService.getOneUser({id: req.params.id});
        }
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    }
}, responseMiddleware);

router.post('/',
 userValidationRules(),
 createUserValid,
 (req, res, next) => {
    try {
        if (!res.err) {
            res.data = UserService.createUser(req.body);
        }
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    } finally {
        next()
    }
}, responseMiddleware);

router.put('/:id',
userValidationRules(),
updateUserValid,
(req, res) => {
    try {
        if (!res.err) {
            res.data = UserService.updateUser(req.params.id, req.body);
        }
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    }
});

router.delete('/:id', (req, res) => {
    try {
        if (!res.err) {
            res.data = UserService.delete({id: req.params.id});
        }
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    }
}, responseMiddleware);


module.exports = router;