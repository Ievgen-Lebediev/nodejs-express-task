const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = FighterService.getAllFighters();
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    } finally {
        next()
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = FighterService.getOneFighter({id: req.params.id});
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    } finally {
        next()
    }
}, responseMiddleware);

router.post('/', (req, res, next) => {
    try {
        res.data = FighterService.createFighter(req.body);
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    } finally {
        next()
    }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
    try {
        res.data = FighterService.updateFighter(req.params.id, req.body);
    } catch(err) {
        res.err = err;
        res.err.status = 404;
    } finally {
        next()
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = FighterService.delete(req.params.id);
    } catch(err) {
        res.err = err;
        res.err.status = 404;    } finally {
        next()
    }
}, responseMiddleware);

module.exports = router;